<?php

namespace GameServer;

use Illuminate\Support\ServiceProvider;

class GameServerServiceProvider extends ServiceProvider
{
    protected $defer = false;

    protected $aliases = [];

    public function register()
    {
        $this->app->singleton('gameserver', function ()
        {
            return new GameServerApi;
        });

        $this->app->alias('gameserver', 'GameServer\GameServerApi');
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/gameserver.php' => config_path('gameserver.php'),
        ]);
    }

    public function provides()
    {
        return ['gstech'];
    }
}