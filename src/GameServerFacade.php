<?php namespace GameServer;

use Illuminate\Support\Facades\Facade;

class GameServerFacade extends Facade {

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor() { return 'gameserver'; }

}