<?php namespace GameServer;

use BackOffice\Models\Machine;
use Config;
use Log;

class GameServerApi {
    protected $host;
    protected $accessKey;
    protected $timeout;
    public $demo = 0;

    function __construct()
    {
        $this->host = Config::get('gameserver.host');
        $this->accessKey = $this->slotsAuth();
        $this->timeout = 20;
    }

    public function demo($demo)
    {
        $this->demo = $demo;
    }

    public function slotsAuth()
    {
        $login = env('GAMESERVER_LOGIN');
        $pass = env('GAMESERVER_PASS');

        $headers = array(
            'Authorization: Basic ' . base64_encode($login . ":" . $pass),
        );

        $request = curl_init($this->host . '/api/slots.auth');
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($request, CURLOPT_FAILONERROR, true);
        curl_setopt($request, CURLOPT_TIMEOUT, $this->timeout);

        $response = curl_exec($request);
        $error = curl_error($request);
        $info = curl_getinfo($request);
        curl_close($request);

        if($response === false)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE CURL ERROR: ' . $error);

            return false;
        }

        if($info['http_code'] !== 200)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ' . $info['http_code'] . ' :' . $response);

            return false;
        }

        $responseObject = json_decode($response);

        if($responseObject === null)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE INVALID JSON ' . json_last_error() . ':' . $response);

            return false;
        }

        if(empty($responseObject->response->accessKey))
        {
            dd($responseObject->response->accessKey);
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ERROR :' . $response);

            return false;
        }

        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE:' . $response);

        return $responseObject->response->accessKey;
    }

    public function getSlots($slotUUID)
    {
        $params = [
            'accessKey' => $this->accessKey,
            'slotId'    => $slotUUID,
        ];

        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' REQUEST: ', $params);

        $request = curl_init($this->host . '/api/slots.get?' . http_build_query($params));

        curl_setopt($request, CURLOPT_HTTPGET, true);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_FAILONERROR, true);
        curl_setopt($request, CURLOPT_TIMEOUT, $this->timeout);

        $response = curl_exec($request);
        $error = curl_error($request);
        $info = curl_getinfo($request);

        curl_close($request);

        if($response === false)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE CURL ERROR: ' . $error);

            return false;
        }

        if($info['http_code'] !== 200)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ' . $info['http_code'] . ' :' . $response);

            return false;
        }

        $data = json_decode($response, true);

        if($data === null)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE INVALID JSON ' . json_last_error() . ':' . $response);

            return false;
        }

        if(isset($data['error']))
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ERROR :' . $response);

            return false;
        }

        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE:', $data);

        return $data['response']['items'][0];
    }

    public function acquireSession($params)
    {
        $params_request = [
            'accessKey'           => $this->accessKey,
            'playerId'            => $params['user'],
            'machineId'           => $params['machineId'],
            'betHttpCallback'     => $params['betHttpCallback'],
            'winHttpCallback'     => $params['winHttpCallback'],
            'refundHttpCallback'  => $params['refundHttpCallback'],
            'balanceHttpCallback' => $params['balanceHttpCallback'],
            'merchantKey'         => $params['merchantKey'],
            'sessionToken'        => $params['sessionToken'],
            'closeHttpCallback'   => $params['closeHttpCallback'],
            'disableHistoryLog'   => $params['disableHistoryLog'],
            'disableAnalyticsLog' => $params['disableAnalyticsLog'],
        ];

        if(isset($params['bonus']) AND $params['bonus'] != false)
        {
            $params_request = array_merge($params_request, $params['bonus']);
        }

        if(isset($params['maxWin']))
        {
            $params_request['maxWin'] = $params['maxWin'];
        }

        if($this->demo)
        {
            $request = curl_init($this->host . '/api/slots.acquireSessionTransfer');
            $params_request['balance'] = isset($params['balance']) ? $params['balance'] : 2000;
        }
        else
        {
            $request = curl_init($this->host . '/api/slots.acquireSessionSeamless');
        }

        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' REQUEST: ', $params_request); //обяз параметры проверить

        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($params_request));
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_FAILONERROR, true);
        curl_setopt($request, CURLOPT_TIMEOUT, $this->timeout);

        $response = curl_exec($request);
        $error = curl_error($request);
        $info = curl_getinfo($request);

        curl_close($request);

        if($response === false)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE CURL ERROR: ' . $error);

            return false;
        }

        if($info['http_code'] !== 200)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ' . $info['http_code'] . ' :' . $response);

            return false;
        }

        $data = json_decode($response, true);

        if($data === null)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE INVALID JSON ' . json_last_error() . ':' . $response);

            return false;
        }
        if(isset($data['error']))
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ERROR :' . $response);

            return false;
        }
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE:', $data);

        return $data['response'];
    }

    public function createMachine($slotUUID, $sessionUUID, $tag = '', $payout)
    {
        $params = [
            'accessKey' => $this->accessKey,
            'slotId'    => $slotUUID,
            'count'     => 1,
            'payout'    => $payout,
        ];

        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' REQUEST: ', $params);
        $request = curl_init($this->host . '/api/slots.addMachine');

        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_FAILONERROR, true);
        curl_setopt($request, CURLOPT_TIMEOUT, $this->timeout);

        $response = curl_exec($request);
        $error = curl_error($request);
        $info = curl_getinfo($request);

        curl_close($request);

        if($response === false)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE CURL ERROR: ' . $error);

            return false;
        }

        if($info['http_code'] !== 200)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ' . $info['http_code'] . ' :' . $response);

            return false;
        }

        $data = json_decode($response, true);

        if($data === null)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE INVALID JSON ' . json_last_error() . ':' . $response);

            return false;
        }
        if(isset($data['error']))
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ERROR :' . $response);

            return false;
        }
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE:', $data);

        $this->setMachineBO($data['response']['items'][0], $sessionUUID, $tag, $payout);


        return $data['response']['items'][0];
    }

    public function setMachineBO($machine, $sessionUUID, $tag = '', $payout)
    {
        Machine::create(
            [
                'machine_id' => $machine['id'],
                'slot'       => $machine['slot']['id'],
                'active'     => 1,
                'is_demo'    => $this->demo,
                'session_id' => $sessionUUID,
                'tag'        => $tag,
                'payout'     => $payout,
            ]
        );
    }

    public function getMachine($slotUUID, $sessionUUID, $tag = '', $payout = null)
    {
        $slot = $this->getSlots($slotUUID);
        if($slot === false)
        {
            return false;
        }

        if(null === $payout)
        {
            if(!empty($slot['payout']))
            {
                $payout = $slot['payout'];
            }
        }

        $payouts = $slot['module']['payouts'];

        if(sizeof($payouts) == 0)
        {
            return false;
        }

        if(!in_array($payout, $payouts))
        {
            sort($payouts);
            $i = 0;
            while($i < sizeof($payouts) AND $payouts[$i] <= $payout)
            {
                $i++;
            }
            if($i != 0)
            {
                $i--;
            }
            $payout = $payouts[$i];
        }

        $machinesBO = Machine::where('slot', $slotUUID)
            ->where('session_id', $sessionUUID)
            ->where('tag', $tag)
            ->where('is_demo', $this->demo)
            ->where('payout', $payout)
            ->first();

        if($machinesBO !== null)
        {
            $machinesBO->updated_at = date('Y-m-d H:i:s');
            $machinesBO->save();

            return [
                'id' => $machinesBO['machine_id']
            ];
        }

        $machines = $this->getMachines($slotUUID);

        if($machines === false)
        {
            return false;
        }

        if(count($machines) > 0)
        {
            foreach($machines as $machine)
            {
                if($machine['state'] == 1 AND $machine['payout'] == $payout)
                {
                    $machinesBO = Machine::where('machine_id', $machine['id'])
                        ->where('is_demo', $this->demo)
                        ->where('tag', $tag)
                        ->first();
                    if(!is_null($machinesBO))
                    {
                        $machinesBO->updated_at = date('Y-m-d H:i:s');
                        $machinesBO->session_id = $sessionUUID;
                        $machinesBO->payout = $payout;
                        $machinesBO->save();

                        return $machine;
                    }
                }
            }
        }

        return $this->createMachine($slotUUID, $sessionUUID, $tag, $payout);
    }

    public function getMachines($slotUUID)
    {
        $params = [
            'accessKey' => $this->accessKey,
            'slotId'    => $slotUUID,
        ];
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' REQUEST: ', $params);
        $request = curl_init($this->host . '/api/slots.getMachines?' . http_build_query($params));

        curl_setopt($request, CURLOPT_HTTPGET, true);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_FAILONERROR, true);
        curl_setopt($request, CURLOPT_TIMEOUT, $this->timeout);

        $response = curl_exec($request);
        $error = curl_error($request);
        $info = curl_getinfo($request);
        curl_close($request);

        if($response === false)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE CURL ERROR: ' . $error);

            return false;
        }

        if($info['http_code'] !== 200)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ' . $info['http_code'] . ' :' . $response);

            return false;
        }

        $data = json_decode($response, true);

        if($data === null)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE INVALID JSON ' . json_last_error() . ':' . $response);

            return false;
        }
        if(isset($data['error']))
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ERROR :' . $response);

            return false;
        }

//        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE:', $data);

        return $data['response']['items'];
    }

    public function historyByUser($userUUID)
    {
        $params = [
            'accessKey' => $this->accessKey,
            'user'      => $userUUID,
        ];
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' REQUEST: ', $params);
        $request = curl_init($this->host . '/api/slots.historyByUser?' . http_build_query($params));

        curl_setopt($request, CURLOPT_HTTPGET, true);
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_FAILONERROR, true);
        curl_setopt($request, CURLOPT_TIMEOUT, $this->timeout);

        $response = curl_exec($request);
        $error = curl_error($request);
        $info = curl_getinfo($request);

        curl_close($request);

        if($response === false)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE CURL ERROR: ' . $error);

            return false;
        }

        if($info['http_code'] !== 200)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ' . $info['http_code'] . ' :' . $response);

            return false;
        }

        $data = json_decode($response, true);

        if($data === null)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE INVALID JSON ' . json_last_error() . ':' . $response);

            return false;
        }
        if(isset($data['error']))
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ERROR :' . $response);

            return false;
        }
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE:', $data);

        return $data['response']['items'];
    }

    public function closeSession($sessionUUID)
    {
        $params = [
            'accessKey' => $this->accessKey,
            'sessionId' => $sessionUUID,
        ];
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' REQUEST: ', $params);
        $request = curl_init($this->host . '/api/slots.closeSession?' . http_build_query($params));

        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($request, CURLOPT_FAILONERROR, true);
        curl_setopt($request, CURLOPT_TIMEOUT, $this->timeout);

        $response = curl_exec($request);
        $error = curl_error($request);
        $info = curl_getinfo($request);

        curl_close($request);

        if($response === false)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE CURL ERROR: ' . $error);

            return false;
        }

        if($info['http_code'] !== 200)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ' . $info['http_code'] . ' :' . $response);

            return false;
        }

        $data = json_decode($response, true);

        if($data === null)
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE INVALID JSON ' . json_last_error() . ':' . $response);

            return false;
        }
        if(isset($data['error']))
        {
            Log::error(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE ERROR :' . $response);

            return false;
        }
        Log::info(__CLASS__ . ' ' . __FUNCTION__ . ' GS: ' . $this->host . ' RESPONSE:', $data);

        return $data['response'];
    }
}