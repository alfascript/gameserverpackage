<?php

return [

    'host'      => env('GAMESERVER_HOST', '127.0.0.1'),
    'accessKey' => env('GAMESERVER_ACCESSKEY', ''),
];